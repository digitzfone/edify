root = exports ? this

root.PrayerRequest = new Mongo.Collection 'PrayerRequest'

root.PrayerRequest.attachSchema(
  new SimpleSchema(
    title:
      type: String
      label: "Title"

    description:
      type: String
      label: "Description"
      autoform:
        rows: 5

    createdAt:
      type: Date
      autoValue: ->
        return new Date()
      autoform:
        omit:true

    userId:
      type: String
      autoValue: ->
        return Meteor.userId()
      autoform:
        omit: true
  )
)

# Collection2 already does schema checking
# Add custom permission rules if needed
root.PrayerRequest.allow(
  insert : -> true

  update : -> true

  remove : -> true
)
