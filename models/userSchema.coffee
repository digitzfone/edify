Schema = {}

Schema.UserProfile = new SimpleSchema(
  firstName:
    type: String
    label: 'First Name'
    regEx: /^[a-zA-Z-]{2,25}$/
    optional: true
    autoform:
      'label-type': 'floating'
      placeholder: 'First Name'
  lastName:
    type: String
    label: 'Last Name'
    regEx: /^[a-zA-Z-]{2,25}$/
    optional: true
    autoform:
      'label-type': 'floating'
      placeholder: 'Last Name'
  nickname:
    type: String
    label: "Nick Name")


Schema.User = new SimpleSchema(
  emails:
    type: [ Object ]
    optional: true
  'emails.$.address':
    type: String
    regEx: SimpleSchema.RegEx.Email
  'emails.$.verified': type: Boolean
  createdAt: type: Date
  profile:
    type: Schema.UserProfile
    optional: true
  services:
    type: Object
    optional: true
    blackbox: true)

Meteor.users.attachSchema Schema.User