Router.route '/user/settings', (->
  if Meteor.userId()
    this.render 'userSetting'
  else
    this.redirect 'home'
), name: 'userSetting'