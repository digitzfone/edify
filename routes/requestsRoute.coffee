Router.route '/prayer-request', (->
  @render 'prayerRequest'
  SEO.set title: 'Prayer Requests -' + Meteor.App.NAME
  return
), name: 'prayerRequest'