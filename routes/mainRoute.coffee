Router.route '/', (->
  @render 'home'
  SEO.set title: 'Home -' + Meteor.App.NAME
  return
), name: 'home'