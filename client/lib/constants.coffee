# Define App Constants
if Meteor.App
  throw new (Meteor.Error)('Meteor.App already defined? see client/lib/constants.js')
Meteor.App =
  NAME: 'Scuttle Biz'
  DESCRIPTION: 'A site devoted to edification of others'